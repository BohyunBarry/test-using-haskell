
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author grahamm
 */
public class part2 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Please enter a file name to read from: ");
        String filename = keyboard.nextLine();

        try {
            Scanner myFile = new Scanner(new File(filename));
            int lineCount = 0;
            while (myFile.hasNextLine()) {
                lineCount++;
                String line = myFile.nextLine();
                String[] data = line.split(" ");
                if (data.length == 2) {
                    try {
                        int a = Integer.parseInt(data[0]);
                        int b = Integer.parseInt(data[1]);
                        //int result = getModulo(a, b);
                        //System.out.println("The result of " + a + " modulo " + b + " is " + result);
                    } catch (NumberFormatException e) {
                        System.out.println("An exception occurred while parsing numbers from line " + lineCount + " : " + line);
                    }
                }
                else
                {
                    System.out.println("Line " + lineCount + " was not formatted properly: \"" + line + "\"");
                }
            }
        } catch (FileNotFoundException fe) {
            System.out.println("That file was not found in the system, sorry.");
        }
    }
		public static int getModPower(int a, int b, int c)
	{
	int temp = b / 10;
	int remainder = temp -(int)temp;
	int d = temp - remainder;
	int n = remainder * 10;
	int result = (((a ^ 10) / c) ^ d * (a ^ n) / c);
	return result / c;
	}


}
