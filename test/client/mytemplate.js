Session.setDefault('counter', 0);                          // Create a session variable named counter and initialized to 0

Template.mytemplate.helpers({                                // An helper used to retrieve the value of the counter
    counter: function () {
      return Session.get('counter');
    }
});

Template.mytemplate.events({                                  // React to event on mybutton template
    'click #button': function () {                           // The event is 'click' and is related to all button
	console.log('Click!');
      Session.set('counter', Session.get('counter') + 1);   // increment the counter when button is clicked
    }
});