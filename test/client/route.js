Router.configure({
    layoutTemplate: 'main',
    notFoundTemplate: 'notFound',            // The template to render if the route is not found
});
Router.route('/', function () {
    this.render('mytemplate');
});

Router.route('/excercise4', function () {
    this.render('myform');
});

Router.route('/display', function () {
    this.render('paragraph');
});